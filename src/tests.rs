use super::*;

struct TestReader {
    data: Vec<u32>,
}

impl Reader for TestReader {
    fn read_data(&self) -> Result<Vec<u32>, String> {
        Ok(self.data.clone())
    }
}

#[test]
fn test_sweep() {
    let reader = TestReader {
        data: vec![199, 200, 208, 210, 200, 207, 240, 269, 260, 263],
    };
    let sonar = Sonar { reader };
    assert_eq!(sonar.sweep().unwrap(), 7);
}

#[test]
fn test_sliding() {
    let reader = TestReader {
        data: vec![199, 200, 208, 210, 200, 207, 240, 269, 260, 263],
    };
    let sonar = Sonar { reader };
    assert_eq!(sonar.sliding().unwrap(), 5);
}
