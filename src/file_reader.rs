use std::fmt::Debug;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

use crate::Reader;

pub struct FileReader {
    path: String,
}

impl FileReader {
    pub fn new(path: String) -> Self {
        FileReader { path }
    }
}

impl Reader for FileReader {
    fn read_data(&self) -> Result<Vec<u32>, String> {
        read_lines(&self.path)
            .map_err(error_to_string)?
            .map(|maybe_e| maybe_e.map_err(error_to_string))
            .map(|ip| ip.and_then(|val| val.parse::<u32>().map_err(error_to_string)))
            .collect()
    }
}

fn error_to_string<E: Debug>(e: E) -> String {
    format!("Error: {:?}", e)
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn read_from_file() {
        let path = "src/file_reader/test_data.txt".to_string();
        let reader = FileReader { path };
        let data = reader.read_data().unwrap();
        assert_eq!(data, vec![1, 2, 3, 4, 5]);
    }
}
