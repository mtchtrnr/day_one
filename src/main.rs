use file_reader::FileReader;

pub mod file_reader;

#[cfg(test)]
mod tests;

pub trait Reader {
    fn read_data(&self) -> Result<Vec<u32>, String>;
}

pub struct Sonar<R: Reader> {
    reader: R,
}

impl<R: Reader> Sonar<R> {
    pub fn new(reader: R) -> Self {
        Sonar { reader }
    }
}
#[derive(Copy, Clone)]
pub enum LastThree {
    None,
    One(u32),
    Two(u32, u32),
    Three(u32, u32, u32),
}

impl LastThree {
    pub fn update(self, latest: u32) -> Self {
        match self {
            LastThree::None => LastThree::One(latest),
            LastThree::One(one) => LastThree::Two(one, latest),
            LastThree::Two(one, two) => LastThree::Three(one, two, latest),
            LastThree::Three(_, two, three) => LastThree::Three(two, three, latest),
        }
    }

    pub fn total(&self) -> Option<u32> {
        match self {
            LastThree::Three(one, two, three) => Some(one + two + three),
            _ => None,
        }
    }
}

fn sweep_inner(input: (u32, Option<u32>), depth: u32) -> (u32, Option<u32>) {
    let (acc, maybe_last) = input;
    if let Some(last) = maybe_last {
        if depth > last {
            (acc + 1, Some(depth))
        } else {
            (acc, Some(depth))
        }
    } else {
        (acc, Some(depth))
    }
}

fn sliding_inner(input: (u32, LastThree), depth: u32) -> (u32, LastThree) {
    let (mut acc, last_three) = input;
    let old = last_three;
    let new = last_three.update(depth);
    if let Some(old_total) = old.total() {
        if let Some(new_total) = new.total() {
            if new_total > old_total {
                acc += 1
            }
        }
    }
    (acc, new)
}

impl<R: Reader> Sonar<R> {
    pub fn sweep(&self) -> Result<u32, String> {
        Ok(self
            .reader
            .read_data()?
            .into_iter()
            .fold((0, Option::None), sweep_inner)
            .0)
    }

    pub fn sliding(&self) -> Result<u32, String> {
        Ok(self
            .reader
            .read_data()?
            .into_iter()
            .fold((0, LastThree::None), sliding_inner)
            .0)
    }
}

fn main() {
    let reader = FileReader::new("data/day_one.txt".to_string());
    let sonar = Sonar::new(reader);
    match sonar.sliding() {
        Ok(count) => println!("{}", count),
        Err(error) => println!("{}", error),
    }
}
